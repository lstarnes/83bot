#!/usr/bin/env python3
""" Deletes the word given on the command line from words.txt """
import sys

import wordmanager

if len(sys.argv) < 2:
    print("No word specified", file=sys.stderr)
    print("Usage: ", sys.argv[0], "[word]")
    quit(1)

try:
    wordmanager.del_word("words.txt", sys.argv[1])
except wordmanager.WordNotFoundError:
    print("Word not found", file=sys.stderr)
    quit(2)
