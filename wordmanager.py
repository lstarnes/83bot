#!/usr/bin/env python3
""" Functions for manipulating the word and extension corpuses """
import fcntl

class WordError(Exception):
    """ Generic word error """
    pass


class WordExistsError(WordError):
    """ Word already exists in corpus """
    pass


class WordNotFoundError(WordError):
    """ Word not found in corpus """
    pass


def add_word(filename, word):
    """ Adds a word to the corpus file """
    if not word.endswith("\n"):
        word += "\n"

    lowerword = word.lower()

    with open(filename, "r+") as corpus:
        fcntl.flock(corpus, fcntl.LOCK_EX)
        words = corpus.readlines()

        if any(lowerword == x.lower() for x in words):
            raise WordExistsError()

        words.append(word)
        words.sort(key=str.lower)

        corpus.seek(0)
        corpus.writelines(words)
        corpus.truncate()


def del_word(filename, word):
    """ Deletes a word from a corpus file """
    if not word.endswith("\n"):
        word += "\n"

    lowerword = word.lower()

    with open(filename, "r+") as corpus:
        fcntl.flock(corpus, fcntl.LOCK_EX)

        words = corpus.readlines()
        newwords = [x for x in words if lowerword != x.lower()]

        if len(newwords) == len(words):
            raise WordNotFoundError()

        corpus.seek(0)
        corpus.writelines(newwords)
        corpus.truncate()
