#!/usr/bin/env python3
# Copyright (c) 2017 Lee Starnes <lee@canned-death.us>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
""" Generates random 8.3-formatted filenames. """

import random
import string

# Monkey patch rng to use system rng
random.random = random.SystemRandom().random

def read_wordlist(filename, col, max_len):
    """ Read a file and return a list of words. Skips words longer than max_len"""
    with open(filename) as wordfile:
        for word in wordfile:
            word = word.strip().upper()
            if len(word) <= max_len:
                if isinstance(col, list):
                    col.append(word)
                elif isinstance(col, set):
                    col.add(word)
                else:
                    raise ValueError("col is not a list or a set")


class Generator:
    """ Generates random 8.3 filenames """
    validlast = string.digits

    def __init__(self, wordfile="words.txt", extfile="exts.txt", badfile="bad.txt"):
        self.wordlist = list()
        read_wordlist(wordfile, self.wordlist, 8)
        self.extlist = list()
        read_wordlist(extfile, self.extlist, 3)
        self.badwords = list()
        read_wordlist(badfile, self.badwords, 12)

    def __iter__(self):
        return self

    def __next__(self):
        return self.generate_clean()

    def generate(self):
        """ Generate an 8.3 filename """
        filename = ""

        # Generate an 11 char word, we will split it up later into an 8.3
        # filename
        while len(filename) < 11:
            word = random.choice(self.wordlist).upper()
            total_len = len(word) + len(filename)

            if total_len > 13:
                # Even with continuations we can't exceed this
                continue
            elif filename and random.randint(0, 4) == 0:
                # 1 in 5 chance of opportunistic combination
                for i in range(2, 0, -1):
                    if filename[-i:] == word[:i] and total_len - i <= 11:
                        word = word[i:]
                        total_len -= i
                        break
            elif total_len > 11:
                # Continuation not happening, don't exceed 11
                continue

            if (total_len <= 8 or
                    (total_len == 11 and random.randint(0, 19) == 0)):
                # Tack on the word
                filename += word

            # Special cases
            if total_len == 6 and random.randint(0, 9) != 0:
                # 1 in 10 chance of a ~1 substitution
                filename += "~1"
            elif total_len == 7 and random.randint(0, 19) != 0:
                # 1 in 20 chance of continuing, otherwise consider adding ~1 or
                # a character, then the extension
                if random.randint(0, 4) == 0:
                    # 1 in 5 chance of adding ~1
                    filename = filename[:6] + "~1"
                else:
                    filename += random.choice(self.validlast)

                filename += random.choice(self.extlist).upper()
            elif total_len == 8:
                # 1 in 10 chance we change it to ~1
                if random.randint(0, 9) == 0:
                    filename = filename[:6] + "~1"

                # Add extension and call it a day
                filename += random.choice(self.extlist).upper()
            elif total_len == 10 and random.randint(0, 19) == 0:
                # 1 in 20 chance of continuing into the extension, add padding
                filename += word
                filename += random.choice(self.validlast)

        return filename[:8] + "." + filename[-3:]

    def generate_clean(self):
        """ Generate an 8.3 filename that does not contain bad words """
        filename = None
        bad = True
        while bad:
            bad = False
            filename = self.generate()
            filename_check = filename.replace(".", "")
            for badword in self.badwords:
                if badword in filename_check:
                    print("Found badword '{}' in '{}'".format(badword, filename))
                    bad = True
                    break
        return filename

def main():
    """ Main entry point """
    import sys
    gen = Generator()
    count = 1
    if len(sys.argv) > 1:
        count = int(sys.argv[1])
    for _ in range(count):
        word = gen.generate_clean()
        print(word)

if __name__ == '__main__':
    main()
