#!/usr/bin/env python3
""" Deletes the extension given on the command line from exts.txt """
import sys

import wordmanager

if len(sys.argv) < 2:
    print("No extension specified", file=sys.stderr)
    print("Usage: ", sys.argv[0], "[extension]")
    quit(1)

EXTENSION = sys.argv[1]

try:
    wordmanager.del_word("exts.txt", EXTENSION)
except wordmanager.WordNotFoundError:
    print("Extension not found", file=sys.stderr)
    quit(2)
